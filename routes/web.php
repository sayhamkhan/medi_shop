<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.home');
});
Route::get('/index', function () {
    return view('index');
});
Route::get('/', 'frontend\FrontendController@home')->name('home');








//register route
Route::get('/admin/login', 'Admin\LoginController@showLoginForm')->name('login');
Route::post('/admin/login', 'Admin\LoginController@login')->name('admin.login');


Route::post('/register','frontend\FrontendController@showRegister')->name('register');





// Route::post('/register','Auth\RegisterController@userform')->name('userform');







Route::get('/products/category/{id}','frontend\FrontendController@categoryWiseProduct')->name('category.products');


Route::get('/customer','frontend\FrontendController@customer');



Route::get('/registration','frontend\FrontendController@registration');
// Route::get('/loginn','frontend\FrontendController@loginn')->name('log');
Route::post('/rform','frontend\FrontendController@getregistration')->name('insert');






Route::get('/shipping','backend\BackendController@shipping')->name('shipping');
Route::get('/payments','backend\BackendController@payments')->name('payments');





// 	==========================<=========frontend=============================


Route::get('/product','frontend\FrontendController@product')->name('product');
Route::get('/contact','frontend\FrontendController@contact')->name('contact');






//----------------------------- Health-Tipes--------------------------------------

Route::get('/health_tipes_01','frontend\FrontendController@health01')->name('health01');
Route::get('/health_tipes_02','frontend\FrontendController@health02')->name('health02');
Route::get('/health_tipes_03','frontend\FrontendController@health03')->name('health03');

// Route::get('/product','frontend\FrontendController@product')->name('health01');


// Route::post('/register','frontend\FrontendController@showRegister')->name('register');


Auth::routes();

Route::get('/home', 'frontend\FrontendController@home')->name('home');



Route::group( ['middleware' => 'auth'], function() {

//////// logout/////////

Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/logout','Auth\LoginController@logout')->name('logout');


///////////////// cart //////////////////////

Route::post('/add_to_cart/{id}','frontend\CartController@addToCart')->name('cart.add');
Route::get('/cart','frontend\CartController@showCart')->name('carts');
Route::get('/cart/delete/{id}','frontend\CartController@deleteCart')->name('delete.Cart');

////////////// order //////////////////////

//<--Order-page-->
Route::get('/order','frontend\OrderController@showOrder')->name('order');
Route::get('/placeOrder','frontend\OrderController@placeOrder')->name('placeOrder');
Route::get('/deleteOrder/{id}','frontend\OrderController@deleteOrder')->name('deleteOrder');
Route::post('/orders','frontend\OrderController@doOrder')->name('orders.post');
// Route::post('/orders','frontend\OrderController@doOrder')->name('orders.post');






});


Route::group( ['middleware' => 'admin'], function() {

//---------------------------------backend------------------------------------------

Route::get('/admin','backend\BackendController@showmaster')->name('admin');
Route::get('/admin/logout', 'Admin\LoginController@logout')->name('admin.logout');
//Route::get('/admin/logout', 'Admin\LoginController@logoutProcess')->name('admin.logout');
Route::get('/dashboard','backend\BackendController@dashboard')->name('dashboard');

Route::get('/admin/products','backend\BackendController@products')->name('products');
Route::post('/admin/products','backend\BackendController@productform')->name('productform');

Route::post('/category','backend\BackendController@categoryform')->name('categoryform');
Route::get('/category','backend\BackendController@category')->name('category');

Route::post('/category_update/{id}','backend\BackendController@updateCategory')->name('category_update');
Route::get('/category_edit/{id}','backend\BackendController@editCategory')->name('category_edit');
Route::get('/category/delete/{id}','backend\BackendController@deleteCategory')->name('delete.Category');


Route::get('/admin/users','backend\BackendController@users')->name('users');
Route::get('/users/delete/{id}','backend\BackendController@deleteUser')->name('user.Delete');

Route::post('/product_update/{id}','backend\BackendController@updateProduct')->name('product_update');
Route::get('/product_edit/{id}','backend\BackendController@editProduct')->name('product_edit');
Route::get('/product/delete/{id}','backend\BackendController@deleteProduct')->name('delete.Product');

Route::get('admin/orders','backend\BackendController@orders')->name('orders');
Route::post('/orders/complete/{id}','backend\OrderController@OrderDelivered')->name('delivered.order');
Route::get('/order/delete/{id}','backend\OrderController@deleteOrder')->name('delete.Order');
Route::get('/admin/order', 'backend\OrderController@showOrders')->name('order');
Route::get('/admin/details/{id}','backend\OrderdetailController@showdetails')->name('detail');
});

//<--Order-Details -page-->




Route::get('/add_detail/{id}','frontend\DetailsController@detailProduct')->name('details');