<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Orderdetail;

class Order extends Model
{
	protected $fillable=['user_id','total','date','time','payment','delivery'];

    public function orderDetails()
    {
    	return $this->hasMany(Orderdetail::class);
    }
    public function user(){
    	return $this->hasOne(User::Class,'id','user_id');
    }
}
