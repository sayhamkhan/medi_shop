<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable=['name','type','price','quantity','product_image'];

    public function hasCategory(){

    	return $this->hasOne(Category::Class,'id','type');
    }
}


