<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    
    protected $fillable=['product_name','product_id','unit_price','qunt','sub_total','user_id','status'];
    public function hasProducts(){

    	return $this->hasOne(Product::Class,'id','product_id');
    }
}