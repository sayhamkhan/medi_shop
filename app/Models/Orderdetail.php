<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Orderdetail extends Model
{
    	
   protected $fillable=['product_id','order_id','unit_price','qunt','sub_total'];
     
     public function product_relation(){

     	return $this->hasOne(Product::Class,'id','product_id');
     }
}
