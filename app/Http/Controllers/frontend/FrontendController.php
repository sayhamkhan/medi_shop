<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Product;

class FrontendController extends Controller
{

		public  function home() {
	    // return view('frontend.home');
			// $all_products=Product::all();
	    $all_products=Product::orderBy('id','desc')->take(3)->get();
	      // dd($lastProducts);
	      return view ('frontend.home',compact('all_products')); 
		}

	   	public  function product() {

	     $all_products=Product::all();
	     // $all_products=Product::orderBy('id','desc')->take(3)->get();
	      //dd($lastProducts);
	      return view ('frontend.product',compact('all_products')); 




		}

		public function categoryWiseProduct($id)
	    {
	    	// dd($id);
	      $all_products=Product::where('type',$id)->get();
	      //dd($all_products);
	      return view ('frontend.category_product',compact('all_products')); 
	    }





		public function user(){

    	$all_users=User::all();

    

    	return view('backend.customers',compact('all_users'));



    	}

		public  function customer() {
	    return view('frontend.customer');
		}
		public  function registration() {
	    return view('frontend.registration');
		}
		public  function showRegister() {
	    return view('frontend.register');
		}
		public  function contact() {
	    return view('frontend.contact');
		}

//---------------------health-tipes-------------------------------------------------

		public  function health01() {
	    return view('frontend.health.health01');
		}
		public  function health02() {
	    return view('frontend.health.health02');
		}
		public  function health03() {
	    return view('frontend.health.health03');
		}

		

		public  function loginn() {
	    return view('frontend.loginn');
		}


		public  function profile() {
	    return view('frontend.profile');
		}
		public function registerform(Request $request){
		//dd($request->all());
			$data=[
				'first_name'=>$request->input('first_name'),
				'phone_no'=>$request->input('phone_no'),
				'email'=>$request->input('email'),
				'street_address'=>$request->input('street_address'),
				'password_repeat'=>$request->input('password_repeat')
				

			];
			User::create($data);
			return redirect()->back();
		
		}
}
