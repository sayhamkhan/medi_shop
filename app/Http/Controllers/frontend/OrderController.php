<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Orderdetail;
use App\Models\Cart;
use App\Models\Product;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
/*     public function showOrder(){
     	$cart_info=Cart::with('hasProducts')->where('user_id',auth()->user()->id)->where('status','pending')->get();
     	//dd($cart_info);
    return view ('frontend.order',compact('cart_info'));

    }*/

     public function placeOrder(){
     	$cart_info=Cart::with('hasProducts')->where('user_id',auth()->user()->id)->where('status','pending')->get();
     	//dd($cart_info);
    return view ('frontend.order',compact('cart_info'));

    }

    public function doOrder(Request $request){


		$cart_info=Cart::with('hasProducts')->where('user_id',auth()->user()->id)->where('status','pending')->get();

		$total = $cart_info->sum('sub_total');

		
		$order = Order::create([
			'user_id' =>auth()->user()->id,
			'total' =>$total,
			'date' =>$request->input('date'),
			'time' =>$request->input('time'),
			'payment' =>$request->input('payment'),
			'delivery' =>$request->input('delivery')
		]);

		foreach ($cart_info as $product) {
			$orderDetail = Orderdetail::create([
				
				'order_id'=> $order->id,
				'product_id'=> $product->product_id,
				'unit_price'=> $product->unit_price,
				'sub_total' => $product->sub_total,
				'qunt'      => $product->qunt
			]);


			
			$product_quantity=Product::where('id',$product->product_id)->first();
			$order_quantity=Orderdetail::where('product_id',$orderDetail->product_id)->sum('qunt');
			$stock_count=Orderdetail::where('product_id',$product->product_id)->sum('qunt');
			
			$quantity=$product_quantity->quantity - $product->qunt;

				Product::where('id',$product->product_id)->Update([
					
					'quantity' =>$quantity
				]);
		}
		

		Cart::where('user_id',auth()->user()->id)->where('status','pending')->delete();
        session()->flash('message','Thank you for order!');
    	
	    $all_products=Product::orderBy('id','desc')->take(3)->get();
	      // dd($lastProducts);
	      return view ('frontend.home',compact('all_products')); 
    }
    
    public function deleteOrder($id){
    	DB::table('carts')->where('id',$id)->delete();
    	return redirect()->back();
    }

}
