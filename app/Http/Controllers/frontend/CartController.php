<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Product;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
  public function showCart(){
    $user_id= (auth()->user()->id);
    //dd($user_id);
    $all_carts=Cart::with('hasProducts')->where('user_id', $user_id)->get();
       //dd($all_carts);
    return view ('frontend.cart',compact('all_carts'));
  }

  public function addToCart(Request $request, $id)
  {
    $user=(auth()->user()->id);
    $product=Product::where('id', $id)->first();
    $cart=Cart::where('product_id', $id)->where('user_id', $user)->first();
/*  $ext_user=($cart->user_id);
    $fn_user=($ext_user=$user);*/
    if (auth::user()) {
      if (!empty($cart)) {
      /*  echo "update";*/
        $new_qty=$cart->qunt+$request->input('quantity');

        Cart::where('product_id', $cart->product_id)
          ->update([
          'qunt'=>$new_qty,
          'sub_total'=>$new_qty*$product->price,
        ]);
      }
      else{
        Cart::create([
          'product_id'=>$id,
          'product_name'=>($product->name),
          'unit_price'=>($product->price),
          'qunt'=>$request->input('quantity'),
          'sub_total'=>$request->input('quantity')*$product->price,
          'user_id'=>auth()->user()->id,
        ]);
      }
      $all_carts=Cart::where('user_id', $user)->get();
     //return view ('frontend.cart',compact('all_carts'));
    }
      return redirect()->back();
    // $all_carts=Cart::where('product_id',$id)->get();
    // return view ('frontend.cart',compact('all_carts'));
  }

    
 


public function updateCart(Request $request, $id){

  $cart =Cart::find($id);
  if(!is_null($cart)){
    $cart->product_quantity = $request->product_quantity;

    $cart->save();
  }
  else {
    return redirect()->route('cart.add');

  }
  // session()->flash('message', 'Cart item has updated successfully !!');
  // return back();

}
  public function deleteCart($id){
  //DB::table('carts')->where('product_id',$id)->delete();
    $product_id=Cart::find($id);
    $product_id->delete();
    //dd($product_id);
  return redirect()->back();
 }

}










// if(auth::user()){

    //  $all_products=Product::select('price')->where('id',$id)->first();

    //  $checkCart=Cart::where('product_id',$id)
    //  ->where('user_id',auth()->user()->id)->where('status','pending')->first();
    //  if($checkCart)
    //  {
    //   $new_qty=$checkCart->qunt+1;
    //   Cart::where('id',$checkCart->id)
    //  ->update([
    //   'qunt'=>$new_qty,
    //   'sub_total'=>$new_qty*$product->price

    // ]);
     
    //  }
    
    //  else{
    //    Cart::create([
    //     'product_id'=>$id,
    //     'unit_price'=>$product->price,
    //     'qunt'=>1,
    //     'sub_total'=>1*$product->price,
    //     'user_id'=>auth()->user()->id,
    //   ]);
    //  }

