<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Auth\Middleware\Authenticate ;
use App\Http\Controllers\Controller;

use App\Notifications\VerifyRegistration;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Admin;
use Auth;


class LoginController extends Controller
{

  public function showLoginForm()
  {
    return view('backend.login');
  }


  public function login(Request $request)
  {

    $login = $request->only('email','password');

    if (Auth::attempt($login)) {

      //dd(auth()->user());
      
      if(auth()->user()->role=='admin'){
        return redirect('/admin');
      }
    }
    else{
      session()->flash('message', 'Invalid');
      return redirect()->back();
    }
  }
/*  public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }*/

  public function logout()
  {
    Auth::logout();
    return redirect()->back();
  }

}
