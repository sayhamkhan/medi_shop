<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\User;

class OrderController extends Controller
{
     public function showOrders()
     {
     	$all_orders=Order::with('user')->get();
     	$all_orders=Order::orderBy('id','desc')->paginate(5);
     	return view('backend.order',compact('all_orders'));
   
    } 

    public function deleteOrder($id){
  //DB::table('carts')->where('product_id',$id)->delete();
    $order_id=Order::find($id);
    $order_id->delete();
    //dd($product_id);
  return redirect()->back();
 }

 	public function OrderDelivered($id){
 		$order=Order::find($id);
 		if ($order->delivered) {
 			$order->delivered=0;
 		}
 		else{
 			$order->delivered=1;
 		}
 		$order->save();
 		return redirect()->back()->with('message','Order Deliverd');
 		return back();
 	}
}

