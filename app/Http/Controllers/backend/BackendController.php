<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\User;
use App\Models\Order;
use App\Models\Shipping;
use App\Models\Payment;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Image;


class BackendController extends Controller
{
  public function showMaster(){
    
   return view('backend.dashboard');


 }


 public function customers(){

   return view('backend.customers');


 }


 public function orders(){

   return view('backend.orders');


 }


 public function dashboard(){

   return view('backend.dashboard');


 }



 public function products(){

  $categories=Category::all();

  $all_products=Product::with('hasCategory')->orderBy('id','desc')->paginate(5);

// dd($all_products);



  return view('backend.products',compact('categories','all_products'));



}

public function editProduct($id)
{
  $edit=Product::where('id',$id)->first();
  $categories=Category::all();
  return view('backend.editProduct',compact('edit','categories'));
}


public function updateProduct(Request $request,$id){


  Product::where('id',$id)->update([

    'name'=>$request->input('name'),
    'type'=>$request->input('category_id'),
    'price'=>$request->input('price'),
    'quantity'=>$request->input('quantity'),
    // 'product_image'=>$imageName,


  ]);
  return redirect('/admin/products')->with('message','Product Updated Successfully');

}

public function deleteProduct($id){
  //DB::table('carts')->where('product_id',$id)->delete();
  $product_id=Product::find($id);
  $product_id->delete();
    //dd($product_id);
  return redirect()->back();
}

public function users(){

  $users=User::all();
  $all_users=User:: orderBy('id','desc')->paginate(5);
  return view('backend.customers',compact('all_users'));



}


public function deleteUser($id){
  //DB::table('carts')->where('product_id',$id)->delete();
  $user_id=User::find($id);
  $user_id->delete();
    //dd($product_id);
  return redirect()->back();
}
/////////////////category////////////////////////////////////////


public function category(){

  $all_categories = Category::orderBy('id')->get();



  //dd($all_categories);


  return view('backend.category', compact('all_categories'));


    	// $all_categories=Category::all();
    	// return view('backend.category',compact('all_categories'));



}
public function editCategory($id){


  $edit=Category::where('id',$id)->first();
  return view('backend.editcategory',compact('edit'));


}


public function updateCategory(Request $request,$id){


  Category::where('id',$id)->update([

    'name'=>$request->input('name'),
    // 'image'=>$request->input('image'),
    'parent_id'=>$request->input('parent_id'),


  ]);
  return redirect('/category')->with('message','Category Updated Successfully');

}

public function deleteCategory($id){
  //DB::table('carts')->where('product_id',$id)->delete();
  $Category_id=Category::find($id);
  $Category_id->delete();
    //dd($product_id);
  return redirect()->back();
}

public function shipping(){

 return view('backend.shipping');


}


public function payments(){

 return view('backend.payments');


}


public function productform(Request $request){

    	// dd($request->all());

  $imageName='';
  if($request->hasfile('product_image'))
  {
    $photo=$request->file('product_image');
         // dd($request->file('product_image')->getClientOriginalName());
    $imageName=uniqid('pImage_',true).str_random(10).'.'.$photo->getClientOriginalName();
    $photo->storeAs('product_image',$imageName);
  }

  $data=[

    'name'=>$request->input('name'),
    'type'=>$request->input('category_id'),
    'price'=>$request->input('price'),
    'quantity'=>$request->input('quantity'),
    'product_image'=>$imageName,
  ];


// dd($data);


  Product::create($data);
  return redirect()->back();


     // dd(Product::create($data));


}


public function categoryform(Request $request){

      // dd($request->all());

  $data=[
    'name'=>$request->input('name'),

    'parent_id'=>$request->input('parent_id'),

  ];


  $all_category = new Category();
  $all_category->name = $request->name;

  $all_category->parent_id = $request->parent_id;

  $all_category->save();

  session()->flash('success', 'A new category has added successfully !!');
  return redirect()->route('category');
  Category::create($data);

      // return redirect()->back();
}
}



// public function create(Request $request)
//     {

//       Product::create([
//           'name'=>$request->input('product_name'),
//           'price'=>$request->input('price'),
//           'category_id'=>$request->input('category_id'),
//       ]);
