@extends('backend.master')
@section('content') 
      <style>
  h1{
    text-align: center;
  };
  
</style>


               <!-- Button trigger modal -->


<h1>Products Details</h1> 
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Add Product
</button> <br><br>          
 <div class="table-responsive" >
                    <table class="table table-striped" id="orderTable">
                      <thead>
                        <tr>
                          <th>Serial</th>
                          <th>Product Name</th>
                          <th>Product Category</th>
                          <th>Product Price</th>
                          <th>Quantity</th>
                          <th>Image</th>
                          <th>Action</th>
                           
                          <th></th>                                                   
                       </tr>
                      </thead>
                      <tbody>

                        @foreach($all_products as $key=>$all_product)
                        <tr>
                          <td>{{$key+1}}</td>
                          <td>{{$all_product->name}}</td>
                          <td>{{$all_product->hasCategory->name}}</td>
                          <td>{{$all_product->price}}</td>
                          <td>{{$all_product->quantity}}</td>
                          <td> <img src="{{url('/product_image/'.$all_product->product_image)}}" style="height: 50px; width: 60px"> </td>
                          
                          <td>
                            
                            <a href="{{route('product_edit',$all_product->id)}}" class="btn btn-info">Update</a>
                            <a href="{{route('delete.Product',$all_product->id)}}" class="btn btn-danger">Delete</a>
                          </td>
                        </tr>

                        @endforeach

                        {{$all_products->links()}}

                      </tbody>
                    </table>
                  </div>






<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> Add Product</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{route('productform')}}" method="post" role="form" enctype="multipart/form-data">
          @csrf()

          <div class="form-group">
            <label for="name">Product Name:</label>
             <input class="form-control" type="text" name="name" id="name"></div>


          <div class="form-group">
            <label for="type">Product's Category: </label>
            <select class="form-control" id="category" name="category_id">
              @foreach($categories as $category)
                <option value="{{$category->id}}">{{$category->name}}</option>
             @endforeach
              

            </select>
          </div>


          <div class="form-group">
            <label for="price">Product Price: </label>
            <input class="form-control" type="text" name="price" id="price">
          </div>


          


          <div class="form-group">
            <label for="quantity">Quantity </label>
              <input class="form-control" type="number" name="quantity" id="quantity">
          </div>



          <div class="form-group">
            <label for="quantity">Add Image </label>
              <input type="file" class="form-control" name="product_image" id="product_image">
          </div>
          
          
          
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </form>
    </div>
  </div>
</div>




@stop

@section('script')
<script>
  $(document).ready(function(){
    $('#orderTable').DataTable();
  });

</script>
@endsection
