 <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Medi-Shop</a>
<!--       <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">
 -->  
 @php                 
$notifys = App\Models\Order::where('delivered',0)->get();
$notify_num= $notifys->count();
@endphp     
<div class="panel panel-default">
  <div class="panel-body">
    <!-- Single button -->
    <div class="btn-group pull-right top-head-dropdown">
      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Notification ({{$notify_num}}) <span class="caret"></span>
      </button>
      <ul class="dropdown-menu dropdown-menu-right">
        @foreach($notifys as $notify)
@php                 
$user_details = App\Models\User::where('id',$notify->user_id)->first();
@endphp
        <li class="notify_row">
          <a href="{{route('detail',$notify->id)}}" class="top-text-block">
            <div class="top-text-heading">{{$user_details->first_name}} {{$user_details->last_name}}   {{$notify->total}}TK</div>
          </a> 
        </li>
        @endforeach
      </ul>
    </div>
  </div>
</div>





     <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
        	@guest()
            <a href="{{ route('login') }}">Sign In</a>
            @endguest


            @auth()
            
            <li><a href="{{route('admin.logout')}}">Logout</a></li>
            
            @endauth
              
        </li>
    </ul>
    </nav>