@extends('backend.master')
@section('content') 
<style>
  h1{
    text-align: center;
  };
  
</style>


 <h1>Category List</h1>               <!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Add New
</button>  
<br><br>         
 <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>Category ID</th>
                          <th>Category Name</th>
                          <!-- <th>Category Image</th> -->
                          <th>Parent Name</th>
                          <th>Action</th>
                          
                           
                          <th></th>                                                   
                       </tr>
                      </thead>
                      <tbody>

                        @foreach($all_categories as $key=>$all_category)
                        <tr>
                          <td>{{$key+1}}</td>
                          <td>{{$all_category->name}}</td>
                          <!-- <td>{{$all_category->description}}</td> -->
                          <!-- <td>{{$all_category->image}}</td> -->

                          <td>@if ($all_category->parent_id == NULL)
                                  Primary Category
                                @else
@php                 

$childs = App\Models\Category::select('name')->where('id',$all_category->parent_id)->get();
@endphp
                                  @foreach($childs as $child)
                                  {{ $child->name }}
                                  @endforeach
                                @endif
                          </td>
                          
                          <td>
                            
                            <a href="{{route('category_edit',$all_category->id)}}" class="btn btn-info">Update</a>
                            <a href="{{route('delete.Category',$all_category->id)}}" class="btn btn-danger">Delete</a>
                          </td>
                        </tr>

                        @endforeach

                      </tbody>
                    </table>
                  </div>






<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> Add Category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{route('categoryform')}}" method="post" role="form">
          @csrf

          <div class="form-group">
            <label for="name">Category Name:</label>
             <input class="form-control" type="text" name="name" id="name"></div>


          <!-- <div class="form-group">
            <label for="price">Category Image: </label>
            <input class="form-control" type="text" name="price" id="price">
          </div>
 -->

          <!-- <div class="form-group">
            <label for="type">Parent Name: </label>
            <input class="form-control" type="dorp-down" name="type" id="type">
          </div> -->
          <div class="form-group">
              <label for="exampleInputPassword1">Parent Category (optional)</label>
              <select class="form-control" name="parent_id">
                <option value="">Please select a Parent category</option>
                @foreach ($all_categories as $all_category)
                  <option value="{{ $all_category->id }}">{{ $all_category->name }}</option>
                @endforeach
              </select>

            </div>

          <!--  -->
          
          
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </form>
    </div>
  </div>
</div>





@stop
