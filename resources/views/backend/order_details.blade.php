@extends('backend.master') 
@section('content')




<div class="container order_wrap">
  <div class="row" id ="divToPrint">

  <div class="col-md-6" >
      <div class="">
        <h3>User Info:</h3>
          <p><b>Customer Name: </b>{{$all_details->user->first_name}}</p>
          <p><b>E-mail: </b>{{$all_details->user->email}}</p>
          <p><b>Address: </b>{{$all_details->user->street_address}}</p>
          <p><b>Phone Number: </b>{{$all_details->user->phone_no}}</p>
      </div>
    </div>
    <div class="col-md-6">
      <div class="">
        <h3>Delivery Info:</h3>
          
          <p><b>Address: </b>{{$all_details->user->street_address}}</p>
          <p><b>Date: </b>{{$all_details->date}}</p>
          <p><b>Time: </b>{{$all_details->time}}</p>
          
      </div>
    </div>
    <div class="col-md-6">
      <div class="">
        <table class="table table-striped">
                      <thead>
                       <tr>
                        <th>Order Id</th>
                        <th>Product Id</th>
                        <th>Unit Price</th>
                        <th>Quantity</th>
                        <th>Sub Total</th>
                      </tr>                                                 
                      </thead>
                      @php
                          $total=0;
                      @endphp

                      <tbody>
                       @foreach($all_details->orderDetails as $data) 
                        <tr>
                          <td>{{$data->order_id}}</td>
                          <td>{{$data->product_relation->name}}</td>
                          <td>{{$data->unit_price}}</td>
                          <td>{{$data->qunt}}</td> 
                          <td>{{$data->sub_total}}</td> 
                        </tr>
                           @php
                          $total=$total+$data->sub_total;
                          @endphp
                          @endforeach
                          <tr>
                            <td></td>
                            <td></td>
                            <td>Total:</td>
                            <td>{{$total}}</td>
                            <td></td>
                          </tr>
                      </tbody>
                    </table>

      </div>
    </div>
    <div class="col-md-6">
      <div class="">
        <h3>Payment Method:</h3>
          
          <p><b>Payment: </b>{{$all_details->payment}}</p>
          
      </div>
    </div>
  </div>
</div>

      <form class="print_order">
        <input class="btn btn-primary" type="button" onClick="PrintDiv();" value="Print This Order">
      </form>

      <form class="delivered" action="{{route('delivered.order', $all_details->id)}}" method="post" role="form">
        @csrf
        @if($all_details->delivered)
        <input type="submit" class="btn btn-danger" value="Cancel">
        @else
        <input type="submit" class="btn btn-success" value="Delivery">
        @endif
      </form>


                @endsection




              <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
                <script language="javascript">
                  function PrintDiv() {
                  var divToPrint = document.getElementById('divToPrint');
                  var popupWin = window.open('', '_blank', 'width=1100,height=700');
                  popupWin.document.open();
                  popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
                  popupWin.document.close();
                }
              </script>

