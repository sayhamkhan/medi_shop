@extends('backend.master')
@section('content') 



        <form action="{{route('product_update',$edit->id)}}" method="post" role="form">
          @csrf



          <div class="form-group">
            <label for="name">Product Name:</label>
             <input class="form-control" type="text" name="name" id="name" value="{{$edit->name}}"></div>


          <div class="form-group">
            <label for="type">Product's Category: </label>
            <select class="form-control" id="category" name="category_id" >
              

                @foreach($categories as $category)
                  <option value="{{$edit->type}}" selected>{{$category->name}}</option>
                @endforeach
              
              }
              
           
              
              

            </select>
          </div>


          <div class="form-group">
            <label for="price">Product Price: </label>
            <input class="form-control" type="text" name="price" id="price" value="{{$edit->price}}">
          </div>


          


          <div class="form-group">
            <label for="quantity">Quantity </label>
              <input class="form-control" type="number" name="quantity" id="quantity" value="{{$edit->quantity}}">
          </div>


<!-- 
          <div class="form-group">
            <label for="quantity">Add Image </label>
              <input type="file" class="form-control" name="product_image" id="product_image" value="{{$edit->image}}">
          </div>
           -->
          
          
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </form>
    </div>
  </div>
</div>




@stop
