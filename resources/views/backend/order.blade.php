@extends('backend.master')
@section('content') 
      <style>
  h1{
    text-align: center;
  };
  
</style>

 <h1>Orders Info </h1>               
 <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>  
                       <tr>
                        <th>Serial</th>
                        <th>Customer Name</th>
                        <th>Total_Price</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Payment</th>
                        <th>Delivery</th>
                        <th>Status</th>
                         <th>Actiont</th>
                      </tr>                                                   
                                          
                      </thead>
                      <tbody>

                        @foreach($all_orders as $key=>$data)
                        <tr>
                          <td>{{$key+1}}</td>
                          <td>{{$data->user->first_name}}</td>
                          <td>{{$data->total}}</td>
                          <td>{{$data->date}}</td>
                          <td>{{$data->time}}</td>
                          <td>{{$data->payment}}</td>
                          <td>{{$data->delivery}}</td> 
                          <td>
                            @if($data->delivered)
                            <button type="button" class="btn btn-success">Delivered</button>
                            @else 
                            <button type="button" class="btn btn-danger">panding</button>
                            @endif
                          </td> 
                          <td> 
                            
                            <a href="{{route('delete.Order',$data->id)}}" class="btn btn-danger">Delete</a>
                            <a href="{{route('detail',$data->id)}}" class="btn btn-primary">view</a>
                          </td>
                        </tr>
                        @endforeach
                          {{$all_orders->links()}}
                      </tbody>
                    </table>
                  </div>
@stop

