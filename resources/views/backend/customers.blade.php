@extends('backend.master') 
@section('content') 

   
<style>
  h1{
    text-align: center;
  };
  
</style>


 <h1>Customer List</h1> 


      <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>User Name</th>
                          <th>Phone Number</th>
                          <th>E-mail</th>
                          <th>Address</th>
                          <th>Action</th>
                          <!-- <th>
                            First name
                          </th>
                          <th>
                            Progress
                          </th>
                          <th>
                            Amount
                          </th>
                          <th>
                            Deadline
                          </th> -->
                        </tr>
                      </thead>
                      <tbody>

                        @foreach($all_users as $key=>$all_user)

                        <tr>
                          <td>{{$key+1}}</td>
                          <td>{{$all_user->first_name}}</td>
                          <td>{{$all_user->phone_no}}</td>
                          <td>{{$all_user->email}}</td>
                          <td>{{$all_user->street_address}}</td>
                          
                          <td>
                            
                            <!-- <a href="" class="btn btn-info">Update</a> -->
                            <a href="{{route('user.Delete',$all_user->id)}}" class="btn btn-danger">Delete</a>
                          </td>
                        </tr>

                        @endforeach

                        
                        {{$all_users->links()}}
                       
                        </tr>
                      </tbody>
                    </table>
           </div>





           @endsection