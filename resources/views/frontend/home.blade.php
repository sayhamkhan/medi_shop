@extends('frontend.master')
@section('content')
<body>
<!-- main wrapper start -->
    <main class="body-bg">

        <!-- slider area start -->
        <section class="slider-area">
            <div class="hero-slider-active slick-arrow-style slick-dot-style">
                <!-- single slider item start -->
                <div class="hero-slider-item">
                    <div class="d-flex align-items-center bg-img h-100" data-bg="{{url('images/img03.jpg')}}">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-10 offset-lg-2 col-md-10 offset-md-2">
                                    <div class="hero-slider-content">
                                        <h2>baby diapers</h2>
                                        <h1>flat 25% off</h1>
                                        <h3>at ৳ 1000-only for today</h3>
                                        <a href="shop.html" class="btn-hero">shop now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- single slider item end -->

                <!-- single slider item start -->
                <div class="hero-slider-item">
                    <div class="d-flex align-items-center bg-img h-100" data-bg="{{url('images/img05.jpg')}}">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-10 offset-lg-2 col-md-10 offset-md-2">
                                    <div class="hero-slider-content">
                                        <h2>pain relief spray</h2>
                                        <h1>sale 20% off</h1>
                                        <h3>at ৳ 700-only for today</h3>
                                        <a href="shop.html" class="btn-hero">shop now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- single slider item start -->
            </div>
        </section>
        <!-- slider area end -->

        <!-- service features start -->
        <section class="service-features pt-50">
            <div class="container">
                <div class="service-features-inner bg-white">
                    <div class="row">
                        <div class="col-lg-4 col-md-4">
                            <div class="single-features-item">
                                <div class="features-icon">
                                    <i class="ion-android-sync"></i>
                                </div>
                                <div class="features-content">
                                    <h5>Free return</h5>
                                    <p>You can exchange or return</p>
                                </div>
                            </div>
                        </div>
                        <div class="col lg-4 col-md-4">
                            <div class="single-features-item">
                                <div class="features-icon">
                                    <i class="ion-social-usd"></i>
                                </div>
                                <div class="features-content">
                                    <h5>Free Shipping</h5>
                                    <p>Free shipping on all order</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="single-features-item">
                                <div class="features-icon">
                                    <i class="ion-help-buoy"></i>
                                </div>
                                <div class="features-content">
                                    <h5>support 24/7</h5>
                                    <p>We support online 24 hours a day</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- service features end -->

        <!-- deals area start -->
        <section class="deals-area pt-50">
            <div class="container">
                <div class="deals-wrapper bg-white">
                    <div class="row">
                        <div class="col-12">
                            <div class="section-header-deals">
                                <div class="section-title-deals">
                                    <h4>Latest Products</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="deals-item-wrapper">
                                <div class="deals-carousel-active slick-arrow-style slick-row-15">
                                    
                                     @foreach($all_products as $key=>$data)

                                    <!-- deals single item start -->
                                    <div class="deals-slider-item">
                                        <div class="deals-item">
                                            <div class="deals-thumb">
                                                <a href="{{route('details', $data->id)}}">
                                                    <img src="{{url('/product_image/'.$data->product_image)}}" style="height: 250px; width: 300px;" alt="">
                                                </a>
                                                <!-- <div class="add-to-links">
                                                    <a href="{{route('orders')}}" data-toggle="tooltip" title="Order Now"><i class="fa fa-list-alt" aria-hidden="true"></i></a>
                                                </div> -->
                                            </div>
                                            <div class="deals-content">
                                                <!-- <div class="ratings">
                                                    <span><i class="ion-android-star"></i></span>
                                                    <span><i class="ion-android-star"></i></span>
                                                    <span><i class="ion-android-star"></i></span>
                                                    <span><i class="ion-android-star"></i></span>
                                                    <span><i class="ion-android-star"></i></span>
                                                </div> -->
                                                <h4 class="product-name"><a href="{{route('details', $data->id)}}">{{$data->name}}</a></h4>
                                                <p class="product-desc">Convenience is next to nothing when your day is crammed with action.</p>
                                                <div class="price-box">
                                                    <span class="price-regular">৳ {{$data->price}}</span>
                                                    <!-- <span class="price-old"><del>$29.99</del></span> -->
                                                </div>
                                                @if($data->quantity == 0)
                                                    <div class="availability mt-10 mb-20">
                                                        <i class="ion-checkmark-circled"></i>
                                                        <span>Out Of Stock</span>
                                                    </div>
                                                @else
                                                    <form action="{{route('cart.add',$data->id)}}" method="post" role='form'>
                                                    @csrf
                                                        <input type="hidden" name="quantity" value="1">
                                                        <button class="btn btn-cart"><i class="ion-bag"></i> Add To Cart</button>
                                                    </form>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <!-- deals single item end -->

                                     @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- deals area end -->

        <!-- banner statistics start -->
        <div class="banner-statistics-area pt-50">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="img-container">
                            <a href="#"><img src="{{url('images/img11.jpg')}}" alt="" style="height: 156px;"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- banner statistics end -->
     

 
   

       

        <!-- feature product area start -->
        
        <!-- feature product area end -->

        <!-- brand logo area start -->
        <div class="brand-logo-area bg-white">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="brand-logo-slider">
                            <div class="brand-logo-carousel">
                                <!-- brand single item start -->
                                <div class="brand-item" >
                                    <img src="{{url('images/logo-02.png')}}" alt="" style="height: 48px; width: 234px;">
                                </div>
                                <!-- brand single item end -->

                                <!-- brand single item start -->
                                <div class="brand-item">
                                    <img src="{{url('images/logo-03.png')}}" alt="" style="height: 48px; width: 234px; padding-right: 10px;">
                                </div>
                                <!-- brand single item end -->

                                <!-- brand single item start -->
                                <div class="brand-item">
                                    <img src="{{url('images/logo-04.png')}}" alt="" style="height: 48px; width: 234px; padding-right: 10px;">
                                </div>
                                <!-- brand single item end -->

                                <!-- brand single item start -->
                                <div class="brand-item">
                                    <img src="{{url('images/logo-02.png')}}" alt="" style="height: 48px; width: 234px; padding-right: 10px;">
                                </div>
                                <!-- brand single item end -->

                                <!-- brand single item start -->
                                <div class="brand-item">
                                   <img src="{{url('images/logo-01.png')}}" alt="" style="height: 48px; width: 234px; padding-right: 10px;">
                                </div>
                                <!-- brand single item end -->

                                <!-- brand single item start -->
                                <div class="brand-item">
                                    <img src="{{url('images/logo-03.png')}}" alt="" style="height: 48px; width: 234px; padding-right: 10px;">
                                </div>
                                <!-- brand single item end -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- brand logo area end -->

    </main>
    <!-- main wrapper end -->
</body>
@stop