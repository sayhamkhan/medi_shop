@extends('frontend.master')
@section('content')



   <!-- main wrapper start -->
    <main class="body-bg">

        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <h1>shop from our store</h1>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Products</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->

        <!-- page main wrapper start -->
        <div class="shop-main-wrapper pt-50 pb-50">
            <div class="container">
                <div class="row">
                    <!-- shop main wrapper start -->
                    <div class="col-lg-12">
                        <div class="shop-product-wrapper">
                            <!-- shop product top wrap start -->
                            <div class="shop-top-bar">
                                <div class="row align-items-center">
                                    <div class="col-lg-7 col-md-6 order-2 order-md-1">
                                        <div class="top-bar-left">
                                            <div class="product-view-mode">
                                                <a class="active" href="#" data-target="grid-view"><i class="fa fa-th"></i></a>
                                                <a href="#" data-target="list-view"><i class="fa fa-list"></i></a>
                                            </div>
                                            <div class="product-amount">
                                               
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-md-6 order-1 order-md-2">
                                        <div class="top-bar-right">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- shop product top wrap start -->

                            <!-- product item list start -->
                            <div class="shop-product-wrap grid-view row">
                                <!-- product single item start -->

                                

                                

                                     @foreach($all_products as $key=>$data)
                                     <div class="col-md-4">
                                    <!-- product grid start -->
                                    <div class="product-item">
                                        <div class="product-thumb">
                                     

                                            <a href="{{route('details', $data->id)}}">
                                                <img src="{{url('/product_image/'.$data->product_image)}}" style="height: 250px; width: 300px;" alt="">
                                            </a>
                                            <!-- <div class="add-to-links">
                                                <a href="{{route('orders')}}" data-toggle="tooltip" title="Order Now"><i class="fa fa-list-alt" aria-hidden="true"></i></a>
                                                
                                            </div> -->
                                        </div>
                                        <div class="product-content">
                                            <h5 class="product-name"><a href="{{route('details', $data->id)}}">{{$data->name}}</a></h5>
                                            <div class="price-box">
                                                <span class="price-regular">৳ {{$data->price}}</span>
                                                <!-- <span class="price-old"><del>৳ 29.99</del></span> -->
                                            </div>
                                            <div class="product-item-action">
                                                <form action="{{route('cart.add',$data->id)}}" method="post" role='form'>
                                                @csrf
                                                    <input type="hidden" name="quantity" value="1">
                                                    <button class="btn btn-cart"><i class="ion-bag"></i> Add To Cart</button>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <!-- product grid end -->

                                    





                                    <!-- product list item end -->
                                    <div class="product-list-item">
                                        <div class="product-thumb">
                                            <a href="{{route('details', $data->id)}}">
                                                <img src="{{url('/product_image/'.$data->product_image)}}" style="height: 270px; width: 350px;" alt="">
                                            </a>
                                           <!--  <div class="add-to-links">
                                                <a href="{{route('orders')}}" data-toggle="tooltip" title="Order Now"><i class="fa fa-list-alt" aria-hidden="true"></i></a>
                                                
                                            </div> -->
                                        </div>
                                        <div class="product-content-list">
                                            <div class="ratings">
                                                <span><i class="fas fa-star"></i></span>
                                                <span><i class="fas fa-star"></i></span>
                                                <span><i class="fas fa-star"></i></span>
                                                <span><i class="fas fa-star"></i></span>
                                                <span><i class="fas fa-star"></i></span>
                                                
                                            </div>
                                            <h5 class="product-name"><a href="{{route('details', $data->id)}}">{{$data->name}}</a></h5>
                                            <div class="price-box">
                                                <span class="price-regular">৳ {{$data->price}}</span>
                                                <!-- <span class="price-old"><del>৳ 29.99</del></span> -->
                                            </div>
                                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Unde perspiciatis
                                                quod numquam, sit fugiat, deserunt ipsa mollitia sunt quam corporis ullam
                                                rem, accusantium adipisci officia eaque.</p>
                                            <div class="product-item-action">
                                                <form action="{{route('cart.add',$data->id)}}" method="post" role='form'>
                                                @csrf
                                                    <input type="hidden" name="quantity" value="1">
                                                    <button class="btn btn-cart"><i class="ion-bag"></i> Add To Cart</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- product list item end -->
                                
                                
                                    @endforeach
                                
                            </div>
                            <!-- product item list end -->

                            <!-- start pagination area -->
                            <div class="paginatoin-area text-center">
                                <!-- <ul class="pagination-box">
                                    <li><a class="previous" href="#"><i class="fas fa-angle-left"></i>Prev</a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a class="next" href="#">Next<i class="fas fa-angle-right"></i></i></a></li>
                                </ul> -->
                            </div>
                            <!-- end pagination area -->
                        </div>
                    </div>
                    <!-- shop main wrapper end -->
                </div>
            </div>
        </div>
        <!-- page main wrapper end -->

    </main>
    <!-- main wrapper end -->







@stop

