@extends('frontend.master')
@section('content')







<!-- page main wrapper start -->
        <div class="shop-main-wrapper pt-50">
            <div class="container">
                <div class="row">
                    <!-- product details wrapper start -->
                    <div class="col-lg-12 order-1 order-lg-2">
                        <!-- product details inner end -->
                        <div class="product-details-inner">
                            <div class="row">
                                <div class="col-lg-5">
                                    <div class="product-large-slider mb-20">
                                        <div class="pro-large-img img-zoom">
                                            <img src="{{url('/product_image/'.$all_products->product_image)}}" alt="" />
                                        </div>
      
                                    </div>
                                    
                                </div>
                                <div class="col-lg-7">
                                    <div class="product-details-des">
                                        <h5 class="product-name">{{$all_products->name}}</h5>
                                        <!-- <div class="ratings">
                                            <span><i class="ion-android-star"></i></span>
                                            <span><i class="ion-android-star"></i></span>
                                            <span><i class="ion-android-star"></i></span>
                                            <span><i class="ion-android-star"></i></span>
                                            <span><i class="ion-android-star"></i></span>
                                            <div class="pro-review">
                                                <span>1 review(s)</span>
                                            </div>
                                        </div> -->
                                        <div class="price-box">
                                            <span class="price-regular">BDT: {{$all_products->price}}</span>
                                            <span class="price-old"><del></del></span>
                                        </div>
                                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
                                            eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
                                            voluptua. Phasellus id nisi quis justo tempus mollis sed et dui. In hac
                                            habitasse platea dictumst.</p>

                                        @if($all_products->quantity == 0)
                                        <div class="availability mt-10 mb-20">
                                            <i class="ion-checkmark-circled"></i>
                                            <span>Out Of Stock</span>
                                        </div>
                                        @else
                                        <div class="availability mt-10 mb-20">
                                            <i class="ion-checkmark-circled"></i>
                                            <span>{{$all_products->quantity}} in stock</span>
                                        </div>
                                        <div class="quantity-cart-box d-flex align-items-center">
                                            <form action="{{route('cart.add',$all_products->id)}}" method="post" role='form'>
                                                @csrf
                                            <div class="quantity " >
                                                <input type="number" name="quantity" style="height: 44px; width: 133px; padding: 0px 30px; border-radius: 3px; border-color:rgb(241,172,43);" required/>
                                            </div>
                                            <br>
                                            <div class="action_link">
                                              <button class="btn btn-cart"><i class="ion-bag"></i>Add to cart</button> 
                                            </div>
                                        </form>
                                        </div>
                                        @endif
                                        
                                        
                                        <!-- <div class="useful-links mt-28">
                                            
                                            <a href="#" data-toggle="tooltip" data-placement="top" title="Order This"><i
                                                    class="fa fa-list-alt"></i>Order This Product</a>
                                        </div> -->
                                        <div class="like-icon mt-20">

                                            <a class="facebook" href="#"><i class="fab fa-facebook-f"></i>like</a>
                                            <a class="twitter" href="#"><i class="fab fa-twitter"></i>tweet</a>
                                            <a class="pinterest" href="#"><i class="fab fa-pinterest-p"></i>save</a>
                                            <a class="google" href="#"><i class="fab fa-google-plus-g"></i>share</a>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- product details inner end -->

                        <!-- product details reviews start -->
                        <div class="product-details-reviews mt-50">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="product-review-info">
                                        <ul class="nav review-tab">
                                            <li>
                                                <a class="active" data-toggle="tab" href="#tab_one">description</a>
                                            </li>
                                            
                                            
                                        </ul>
                                        <div class="tab-content reviews-tab">
                                            <div class="tab-pane fade show active" id="tab_one">
                                                <div class="tab-one">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam
                                                        fringilla augue nec est tristique auctor. Ipsum metus feugiat
                                                        sem, quis fermentum turpis eros eget velit. Donec ac tempus
                                                        ante. Fusce ultricies massa massa. Fusce aliquam, purus eget
                                                        sagittis vulputate, sapien libero hendrerit est, sed commodo
                                                        augue nisi non neque.Cras neque metus, consequat et blandit et,
                                                        luctus a nunc. Etiam gravida vehicula tellus, in imperdiet
                                                        ligula euismod eget. Pellentesque habitant morbi tristique
                                                        senectus et netus et malesuada fames ac turpis egestas. Nam
                                                        erat mi, rutrum at sollicitudin rhoncus</p>
                                                </div>
                                            </div>
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- product details reviews end -->
                    </div>
                    <!-- product details wrapper end -->
                </div>
            </div>
        </div>
        <!-- page main wrapper end -->
    



@stop
