<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  font-family: Ahanori, Helvetica, sans-serif;
  background-color: black;
}

* {
  box-sizing: border-box;
}

/* Add padding to containers */
.container {
  padding-top: 100px;
  padding-bottom: 0px;
  padding-left: 400px;
  padding-right: 400px;
  background-color: white;

}


.name1 {
  width: 50%;
  float: left;
  display: block;
  margin-right: 21px;

}
.name2 {
  width: 46%;
  float: left;
  display: block;

}

/* Full-width input fields */
input[type=text], input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  display: inline-block;
  border-radius: 5px;
  background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

/* Overwrite default styles of hr */
hr {
  border: 1px solid #f1f1f1;
  margin-bottom: 25px;
}

/* Set a style for the submit button */
.registerbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px 20px;
  margin: 8px 0;
  border: 2px solid black;
  border-radius: 5px;
  cursor: pointer;
  width: 30%;
  opacity: 0.9;
}

.registerbtn:hover {
  opacity: 1;
}

/* Add a blue text color to links */
a {
  color: dodgerblue;
}

/* Set a grey background color and center the text of the "sign in" section */
.signin {
  background-color: #f1f1f1;
  text-align: center;
}
</style>
</head>
<body>

<form action="{{route('insert')}}" method="post" role="form">
@csrf
  <div class="container">
   
    <h1 style="color:red;text-align: center; background: green;">Registration Form:</h1>
    <p style="color:green;text-align: center;font-size: 13px">Please fill in this form to create an account.</p>
    <hr>

          <div class="name1">
            
              <label for="fname"><b>First Name</b></label>
              <input type="text" placeholder="First Name" name="fname" required>
          </div>

           <div class="name2">
             
              <label for="lname"><b>Last Name</b></label>
              <input type="text" placeholder="Last Name" name="lname" required>


           </div>



       

    <label for="email"><b>Email</b></label>
    <input type="email" placeholder="Enter Your Email" name="email" required>

    <label for="password"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="password" required>

    <label for="password_repeat"><b>Repeat Password</b></label>
    <input type="password" placeholder="Repeat Password" name="password_repeat" required>
    <hr>
    <p style="font-size: 13px">By creating an account you agree to our <a href="#">Terms & Privacy</a>.</p>

    <button type="submit" class="registerbtn">Register</button>
  </div>
  
  <div class="container signin">
    <p style="margin-top: 0px;padding-top: 0px;">Already have an account? <a href="{{route('log')}}">Sign in</a>.</p>
  </div>
</form>

</body>
</html>
