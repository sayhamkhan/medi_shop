
<nav>
    <!-- Start Header Area -->
    <header class="header-area">
        <!-- main header start -->
        <div class="main-header d-none d-lg-block">
            <!-- header top start -->
            <div class="header-top theme-color">
                <div class="container bdr-bottom">
                    <div class="row align-items-center" style="height: 50px;">
                        <div class="col-lg-6">
                            <div class="header-top-settings">
                               
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="header-links">
                                <ul class="nav justify-content-end">
                                    <li>Welcome to Medi-Shop</li>
                                    @guest()
                                    <li><a href="{{ route('login') }}">Sign In</a></li>
                                    <li><a href="{{ route('register') }}">Create an Account</a></li>
                                    @endguest


                                    @auth()
                                    <li>{{auth()->user()->first_name}}</li>
                                    <li><a href="{{route('logout')}}">Logout</a></li>
                                    @endauth

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- header top end -->

            <!-- header middle area start -->
            <div class="header-middle-area theme-color">
                <div class="container">
                    <div class="row align-items-center">
                        <!-- start logo area -->
                        <div class="col-lg-3">
                            <div class="logo">
                                <a href="{{route('home')}}">
                                    <img style="width: 176px;" src="{{url('images/logo.png')}}" alt="">
                                </a>
                            </div>
                        </div>
                        <!-- start logo area -->

                        <!-- start search box area -->
                        <div class="col-lg-5">
                            <!-- <div class="search-box-wrapper">
                                <form class="search-box-inner">
                                    <input type="text" class="search-field" placeholder="Enter your search key">
                                    <button class="search-btn"><i class="ion-ios-search"></i></button>
                                </form>
                            </div> -->
                        </div>
                        <!-- start search box end -->

                        <!-- mini cart area start -->
                        <div class="col-lg-4">
                            <div class="header-configure-wrapper">
                                <div class="support-inner">
                                    <div class="support-icon">
                                        <i class="ion-android-call"></i>
                                    </div>
                                    <div class="support-info">
                                        <p>Free support call:</p>
                                        <strong>+8801979787771</strong>
                                    </div>
                                </div>
                                <div class="header-configure-area">
                                    <ul class="nav justify-content-end">
                                        <!-- <li>
                                            <a href="{{route('orders')}}">
                                               <i class="far fa-heart"></i>
                                            </a>
                                        </li> -->
                                        @auth()
                                        @php
                            
                                        $cart_data=\App\Models\Cart::where('user_id',auth()->user()->id)->get();
                                        
                                        @endphp
                                        @else
                                        @php
                                        $cart_data=null;
                                       
                                        @endphp
                                        @endauth
                                        <li class="mini-cart-wrap">
                                            <a href="{{route('carts')}}">
                                                <i class="ion-bag">
                                                    <span class="badge cart_badge">{{$cart_data?count($cart_data):''}}</span>
                                                </i>
                                            </a>
                                            
                                            <ul class="cart-list">
                                                @if(!empty($cart_data))
                                             @foreach($cart_data as $all_cart)
                                                <li>
                                                    
                                                    <div class="cart-info">
                                                        <h4><a href="product-details.html">{{$all_cart->product_name}}</a></h4>
                                                        <span class="cart-qty">Quantity: {{$all_cart->qunt}}</span>
                                                        <span>৳ {{$all_cart->sub_total}}</span>
                                                    </div>
                                                    <!-- <div class="del-icon">
                                                        <i class="fa fa-times">
                                                            <a href="{{route('delete.Cart',$all_cart->id)}}"></a>
                                                        </i>
                                                    </div> -->
                                                </li>
                                                @endforeach
                                                @endif
                                                <li>
                                                    <div class="mini-cart-button">
                                                        <a class="check-btn" href="{{route('carts')}}">View Cart</a>
                                                        <a class="check-btn" href="{{route('placeOrder')}}">Order</a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- mini cart area end -->
                    </div>
                </div>
            </div>
            <!-- header middle area end -->
 @php                 
$parents = App\Models\Category::where('parent_id',0)->orderBy('id')->get();
@endphp

            <!-- main menu area start -->
            <div class="main-menu-area bg-white sticky">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-3">
                            <div class="category-toggle-wrap">
                                <div class="category-toggle">
                                    <i class="ion-android-menu"></i>
                                    shop by categories
                                    <span><i class="fa fa-angle-down"></i></span>
                                </div>
                                <nav class="category-menu">
                                    <ul class="categories-list">
                                    @foreach($parents as $parent)
                                        <li class="menu-item-has-children"><a href="#">{{$parent->name}}</a>

@php                 
$childs = App\Models\Category::where('parent_id',$parent->id)->get();
@endphp
                                            <!-- Mega Category Menu Start -->
                                            <ul class="category-mega-menu dropdown">
                                                <li class="menu-item-has-children">
                                                    <a href="shop.html">Types Of {{$parent->name}}</a>
                                                    <ul class="dropdown">

                                                    <!-- child -->
                                                    @foreach($childs as $child)
                                                        <li><a href="{{route('category.products',$child->id)}}">{{$child->name}}</a></li>
                                                        @endforeach

                                                    
                                                    </ul>
                                                </li>
                                                
                                            </ul>



                                        </li>
                                        @endforeach
                                        
                                    </ul>

                                </nav>
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="main-menu home-main">
                                <!-- main menu navbar start -->
                                <nav class="desktop-menu">
                                    <ul>
                                        <li class="active"><a href="{{route('home')}}">Home </a>
                                            
                                        </li>
                                        <li><a href="{{route('product')}}">Products </a> </li>
                                        <li><a href="#">Health Tipes <i class="fa fa-angle-down"></i></a>
                                            <ul class="dropdown">

                                                <li><a href="{{route('health01')}}">Everyday's Health and Nutrition Tips</a></li>
                                                
                                                <li><a href="{{route('health02')}}">Diabetic Food List</a></li>
                                                <li><a href="{{route('health03')}}">Syndrome of Kidney Damage</a></li>
                                                <!-- <li><a href="blog-right-sidebar-2-col.html">How to Protect Cancer Cells</a></li>
                                                <li><a href="blog-grid-full-width.html">How To Prevent Antibiotic Resistance</a></li>
                                                
                                                <li><a href="blog-details.html">Syndrome of Liver Disease</a></li> -->
                                                <!-- <li><a href="blog-details-left-sidebar.html">blog details left sidebar</a></li>
                                                <li><a href="blog-details-audio.html">blog details audio</a></li>
                                                <li><a href="blog-details-video.html">blog details video</a></li>
                                                <li><a href="blog-details-image.html">blog details image</a></li> -->
                                            </ul>
                                        </li>
                                        <li><a href="{{route('contact')}}">Contact us</a></li>
                                    </ul>
                                </nav>
                                <!-- main menu navbar end -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- main menu area end -->
        </div>
        <!-- main header start -->

        <!-- mobile header start -->
        <div class="mobile-header d-lg-none d-md-block sticky">
            <!--mobile header top start -->
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="mobile-main-header">
                            <div class="mobile-logo">
                                <a href="index.html">
                                    <img src="assets/img/logo/logo-black.png" alt="Brand Logo">
                                </a>
                            </div>
                            <div class="mobile-menu-toggler">
                                <div class="mini-cart-wrap">
                                    <a href="cart.html">
                                        <i class="fas fa-cart-plus"></i>
                                    </a>
                                </div>
                                <div class="mobile-menu-btn">
                                    <div class="off-canvas-btn">
                                        <i class="ion-android-menu"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="category-toggle-wrap">
                            <div class="category-toggle">
                                <i class="ion-android-menu"></i>
                                all categories
                                <span><i class="ion-android-arrow-dropdown"></i></span>
                            </div>
                            <nav class="category-menu">
                                <ul class="categories-list">
                                    <li class="menu-item-has-children"><a href="shop.html">Fruits & Vegetables</a>
                                        <!-- Mega Category Menu Start -->
                                        <ul class="category-mega-menu dropdown">
                                            <li class="menu-item-has-children">
                                                <a href="shop.html">Smartphone</a>
                                                <ul class="dropdown">
                                                    <li><a href="shop.html">Samsome</a></li>
                                                    <li><a href="shop.html">GL Stylus</a></li>
                                                    <li><a href="shop.html">Uawei</a></li>
                                                    <li><a href="shop.html">Cherry Berry</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children">
                                                <a href="shop.html">headphone</a>
                                                <ul class="dropdown">
                                                    <li><a href="shop.html">Desktop Headphone</a></li>
                                                    <li><a href="shop.html">Mobile Headphone</a></li>
                                                    <li><a href="shop.html">Wireless Headphone</a></li>
                                                    <li><a href="shop.html">LED Headphone</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children">
                                                <a href="shop.html">accessories</a>
                                                <ul class="dropdown">
                                                    <li><a href="shop.html">Power Bank</a></li>
                                                    <li><a href="shop.html">Data Cable</a></li>
                                                    <li><a href="shop.html">Power Cable</a></li>
                                                    <li><a href="shop.html">Battery</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children">
                                                <a href="shop.html">headphone</a>
                                                <ul class="dropdown">
                                                    <li><a href="shop.html">Desktop Headphone</a></li>
                                                    <li><a href="shop.html">Mobile Headphone</a></li>
                                                    <li><a href="shop.html">Wireless Headphone</a></li>
                                                    <li><a href="shop.html">LED Headphone</a></li>
                                                </ul>
                                            </li>
                                        </ul><!-- Mega Category Menu End -->
                                    </li>
                                    <li class="menu-item-has-children"><a href="shop.html">Fresh Meat</a>
                                        <!-- Mega Category Menu Start -->
                                        <ul class="category-mega-menu dropdown three-column">
                                            <li class="menu-item-has-children">
                                                <a href="shop.html">Smartphone</a>
                                                <ul class="dropdown">
                                                    <li><a href="shop.html">Samsome</a></li>
                                                    <li><a href="shop.html">GL Stylus</a></li>
                                                    <li><a href="shop.html">Uawei</a></li>
                                                    <li><a href="shop.html">Cherry Berry</a></li>
                                                    <li><a href="shop.html">uPhone</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children">
                                                <a href="shop.html">headphone</a>
                                                <ul class="dropdown">
                                                    <li><a href="shop.html">Desktop Headphone</a></li>
                                                    <li><a href="shop.html">Mobile Headphone</a></li>
                                                    <li><a href="shop.html">Wireless Headphone</a></li>
                                                    <li><a href="shop.html">LED Headphone</a></li>
                                                    <li><a href="shop.html">Over-ear</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children">
                                                <a href="shop.html">accessories</a>
                                                <ul class="dropdown">
                                                    <li><a href="shop.html">Power Bank</a></li>
                                                    <li><a href="shop.html">Data Cable</a></li>
                                                    <li><a href="shop.html">Power Cable</a></li>
                                                    <li><a href="shop.html">Battery</a></li>
                                                    <li><a href="shop.html">OTG Cable</a></li>
                                                </ul>
                                            </li>
                                        </ul><!-- Mega Category Menu End -->
                                    </li>
                                    <li class="menu-item-has-children"><a href="shop.html">dairy & eggs</a>
                                        <!-- Mega Category Menu Start -->
                                        <ul class="category-mega-menu dropdown two-column">
                                            <li class="menu-item-has-children">
                                                <a href="shop.html">Smartphone</a>
                                                <ul class="dropdown">
                                                    <li><a href="shop.html">Samsome</a></li>
                                                    <li><a href="shop.html">GL Stylus</a></li>
                                                    <li><a href="shop.html">Uawei</a></li>
                                                    <li><a href="shop.html">Cherry Berry</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-item-has-children">
                                                <a href="shop.html">headphone</a>
                                                <ul class="dropdown">
                                                    <li><a href="shop.html">Desktop Headphone</a></li>
                                                    <li><a href="shop.html">Mobile Headphone</a></li>
                                                    <li><a href="shop.html">Wireless Headphone</a></li>
                                                    <li><a href="shop.html">LED Headphone</a></li>
                                                </ul>
                                            </li>
                                        </ul><!-- Mega Category Menu End -->
                                    </li>
                                    <li><a href="shop.html">Frozen</a></li>
                                    <li><a href="shop.html">Grocery</a></li>
                                    <li><a href="shop.html">Kitchenware</a></li>
                                    <li><a href="shop.html">Tools</a></li>
                                    <li><a href="shop.html">Electronics</a></li>
                                    <li><a href="shop.html">Kitchenware</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- mobile header top start -->
        </div>
        <!-- mobile header end -->
    </header>
    <!-- end Header Area -->

    <!-- off-canvas menu start -->
    <aside class="off-canvas-wrapper">
        <div class="off-canvas-overlay"></div>
        <div class="off-canvas-inner-content">
            <div class="btn-close-off-canvas">
                <i class="ion-android-close"></i>
            </div>

            <div class="off-canvas-inner">
                <!-- search box start -->
                <!-- <div class="search-box-offcanvas">
                    <form>
                        <input type="text" placeholder="Search Here...">
                        <button class="search-btn"><i class="fas fa-search"></i></button>
                    </form>
                </div> -->
                <!-- search box end -->

                <!-- mobile menu start -->
                <div class="mobile-navigation">

                    <!-- mobile menu navigation start -->
                    
                    <!-- mobile menu navigation end -->
                </div>
               



    
</nav>


        

    