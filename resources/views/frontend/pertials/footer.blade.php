  <!--== Start Footer Area Wrapper ==-->
    <footer class="footer-wrapper bg-dark ">

        <!-- footer top area start -->
        <div class="footer-top">
            <div class="container">
                <div class="footer-features-inner">
                    <div class="row">
                        <div class="col-lg-4 col-md-4">
                            <div class="single-features-item footer-feature-item">
                                <div class="features-icon">
                                    <i class="fas fa-tty"></i>
                                </div>
                                <div class="features-content">
                                    <h5>+8801979787771</h5>
                                    <p>Free support line!</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4">
                            <div class="single-features-item footer-feature-item">
                                <div class="features-icon">
                                    <i class="fas fa-mail-bulk"></i>
                                </div>
                                <div class="features-content">
                                    <h5>hello@letslearncoding.co</h5>
                                    <p>Orders Support!</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4">
                            <div class="single-features-item footer-feature-item">
                                <div class="features-icon">
                                    <i class="far fa-clock"></i>
                                </div>
                                <div class="features-content">
                                    <h5>Saturday - Thursday / 10:00 - 18:00</h5>
                                    <p>Working Days/Hours!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer top area start -->

        <!-- footer widget area start -->
        <div class="footer-widget-area">
            <div class="container">
                <div class="footer-widget-inner" style="height: 200px;">
                    <div class="row">
                        <!-- footer widget item start -->
                        <div class="col-lg-3 col-md-6 col-sm-6" style="">
                            <div class="footer-widget-item">
                                <div class="footer-widget-logo">
                                    <a href="{{route('home')}}">
                                        <img src="{{url('images/logo.png')}}" alt="">
                                    </a>
                                </div>
                                <div class="footer-widget-body">
                                    
                                    <h6 class="contact-info-title"></h6>
                                    
                                    
                                </div>
                            </div>
                        </div>
                        <!-- footer widget item end -->

                        <!-- footer widget item start -->
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            
                        </div>
                        <!-- footer widget item end -->

                        <!-- footer widget item start -->
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="footer-widget-item">
                                <div class="footer-widget-title">
                                    <h5>Contact info:</h5>
                                    <p>House 99, Road 4/25, Block A, Banani.</p>
                                </div>
                                <ul class="footer-widget-body">
                                    <div class="footer-social-link">
                                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                                        <a href="#"><i class="fab fa-google-plus-g"></i></a>
                                        <a href="#"><i class="fab fa-twitter"></i></a>
                                        <a href="#"><i class="fab fa-youtube"></i></a>
                                        <a href="#"><i class="fa fa-rss"></i></a>
                                    </div>
                                    
                                </ul>
                            </div>
                        </div>
                        <!-- footer widget item end -->

                        <!-- footer widget item start -->
                        
                        <!-- footer widget item end -->
                    </div>
                </div>
            </div>
        </div>
        <!-- footer widget area end -->

        

        <!-- footer bottom area start -->
        <div class="footer-bottom-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="copyright-text text-center">
                            <p>Copyright © 2019 <a href="#">Md. Sayham Khan</a>. All rights reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer bottom area end -->

    </footer>
    <!--== End Footer Area Wrapper ==-->