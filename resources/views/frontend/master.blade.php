<!DOCTYPE html>
<html class="no-js" lang="zxx">


<!-- Mirrored from demo.hasthemes.com/gengar/gengar/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Mar 2019 06:55:54 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="meta description">
    <title>Medi-Shop</title>

    <!--=== Favicon ===-->
    <link rel="shortcut icon" href="{{url('images/favicon.ico')}}" type="image/x-icon" />

    <!-- Google fonts include -->
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,300i,400,400i,500,500i,700,900" rel="stylesheet">

    <!-- All Vendor & plugins CSS include -->
    <!-- <link href="assets/" rel="stylesheet"> -->
    <link href="{{url('/css/vendor.css')}}" rel="stylesheet">

    <!-- Main Style CSS -->
    <!-- <link href="assets/css/style.css" rel="stylesheet"> -->
    <link href="{{url('/css/home.css')}}" rel="stylesheet">

    <!--[if lt IE 9]>
<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

</head>

<body>

   


     @include('frontend.pertials.navbar')


        @yield('content')
      

  

    <!-- Scroll to top start -->
    <div class="scroll-top not-visible">
        <i class="fa fa-angle-up"></i>
    </div>
    <!-- Scroll to Top End -->

    <!-- All vendor & plugins & active js include here -->
    <!--All Vendor Js -->
    <!-- <script src=""></script> -->
    <script src="{{url('/js/vendor.js')}}"></script>

    <!-- Active Js -->
    <!-- <script src=""></script> -->
    <script src="{{url('/js/active.js')}}"></script>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

</body>

@include('frontend.pertials.footer')
<!-- Mirrored from demo.hasthemes.com/gengar/gengar/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Mar 2019 06:55:55 GMT -->
</html>