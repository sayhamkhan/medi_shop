@extends('frontend.master')
@section('content') 
 @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
        @endif  

<!-- cart main wrapper start -->
        <div class="cart-main-wrapper pt-50 pb-50">
            <div class="container">
                <div class="section-bg-color">
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- Cart Table Area -->
                            <div class="cart-table table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="pro-id">ID</th>
                                            
                                            <th class="pro-title">Product Name</th>
                                            <th class="pro-price">Unit Price</th>
                                            <th class="pro-quantity">Quantity</th>
                                            <th class="pro-subtotal">Total</th>
                                            <th class="pro-remove">Remove</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                      @foreach($all_carts as $key=>$all_cart)
                                        <tr>
                                            <td class="pro-id">{{$key+1}}</td>
                                            
                                            <td class="pro-title"><a href="#">{{$all_cart->product_name}}</a></td>
                                            <td class="pro-price"><span>{{$all_cart->unit_price}}</span></td>
                                            <td class="pro-quantity">
                                                {{$all_cart->qunt}}
                                            </td>
                                            <td class="pro-subtotal"><span>{{$all_cart->sub_total}}</span></td>
                                            <td class="pro-remove">
                                              <a href="{{route('delete.Cart',$all_cart->id)}}" class="btn btn-danger" style="color: red;">Remove</a></td>
                                        </tr>
                                        @endforeach
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                            <!-- Cart Update Option -->
                            <div class="cart-update-option d-block d-md-flex justify-content-between">
                                <div class="apply-coupon-wrapper">
                                     <a href="{{route('product')}}" class="btn btn__bg">Add More</a>
                                </div>
                                <div class="cart-update">
                                    <a href="{{route('placeOrder')}}" class="btn btn__bg">Order This</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- cart main wrapper end -->



</html>       

@stop
