@extends('frontend.master')
@section('content') 

@if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    
<!-- order main wrapper start -->
        <div class="cart-main-wrapper pt-50 pb-50">
            <div class="container">
            	<form action="{{route('orders.post')}}" method="post" role='form'>

            	@csrf
                <div class="section-bg-color">
                	
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- Cart Table Area -->
                            <div class="cart-table table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="pro-thumbnail">Product Name</th>
                                            <th class="pro-title">Unit Price</th>
                                            <th class="pro-price">Quantity</th>
                                            <th class="pro-quantity">Sub Total</th>
                                            <th class="pro-subtotal">Status</th>
                                            <th class="pro-remove">Remove</th>
                                        </tr>
                                       
                                    </thead>
                                    <tbody>
                                    @php
									$total=0;
									@endphp
									<!-- For Loop -->
									@foreach($cart_info as $data)
                                        <tr>
                                            <td class="pro-thumbnail">{{$data->product_name}}</td>
                                            <td class="pro-title">{{$data->unit_price}}</td>
                                            <td class="pro-price"><span>{{$data->qunt}}</span></td>
                                            <td class="pro-quantity">
                                                {{$data->sub_total}}
                                            </td>
                                            <td class="pro-subtotal">{{$data->status}}</td>
                                            <td class="pro-remove"><a href="{{route('deleteOrder',$data->id)}}" class="btn btn-danger" style="color: red;">Delete</a></td>
                                        </tr>
                                    @php
											$total=$total+$data->sub_total;
											@endphp
											@endforeach
									
                                    </tbody>
                                </table>
                            </div>
                            <!-- Cart Update Option -->
                            <div class="cart-update-option d-block d-md-flex justify-content-between">
                                <div class="apply-coupon-wrapper">
                                    
                                </div>

                                <div class="cart-update">
                                    <a href="{{route('product')}}" class="btn btn__bg">Shop More</a>
                                </div>
                            </div>

                            <div class="cart-update-option d-block d-md-flex justify-content-between" style="background-color:#64fcf1;">
                                
                                    <h1 style="text-align: center; margin: auto; color: #c69007;">Order Form</h1>
      
                            </div>


                        <div class="cart-update-option d-block d-md-flex justify-content-between" style="width: 650px;background-color: #e0fffc;">
                                <div class="apply-coupon-wrapper" style="width: 100%; " >
				               <div class="input-group mb-3">
										  <div class="input-group-prepend">
										    <label class="input-group-text" for="payment">Payment Options</label>
										  </div>
										  <select class="custom-select" id="payment" name="payment" type="text" required>
										    <option selected>Choose...</option>
										    <!-- <option value="B-Kash">B-Kash</option> -->
										    <option value="Cash on Delivery">Cash on Delivery</option>
										    
										  </select>
										</div>
                                    <br>

                                    

                                    <div class="input-group mb-3">
										  <div class="input-group-text" for="input-group-prepend">
										    <label for="inputGroupSelect01">Choose Delivery Date</label>
										  </div>
										  <div class="well">
 												<input data-format="dd-MM-yyyy" type="date" id="start" name="date"  style="padding: 4px; width: 420px; padding:8px;border-radius:5px;" />
										</div>
									</div>
                                    <br>
                                    <div class="input-group mb-3">
										  <div class="input-group-prepend">
										    <label class="input-group-text" for="inputGroupSelect01">Choose Delivery Time</label>
										  </div>
										  <div class="well">
 												<input type="time" id="inputGroupSelect01" name="time" min="9:00" max="18:00" class="custom-select" required style="width:420px;">
										</div>
									</div>
                                    <br>

                                    <div class="input-group mb-3">
										  <div class="input-group-prepend">
										    <label for="id" class="input-group-text" >Delivery Option</label>
										  </div>
										  <select class="custom-select" type="text" name="delivery" id="id" type="text" required>
										    <option selected>Choose...</option>
										    <option value="Pickup">Pickup</option>
										    <option value="Delivery">Delivery</option>
										    
										  </select>
										</div>
                                    <br>	
							</div>
						</div>              
                               
                            

                    <div class="row" style="margin: left;">
                        <div class="col-lg-5 ml-auto">
                            <!-- Cart Calculation Area -->
                            <div class="cart-calculator-wrapper">
                                <div class="cart-calculate-items">
                                    <h3>Cart Totals</h3>
                                    <div class="table-responsive">
                                        <table class="table">

                                            <tr>
                                                <td>Sub Total</td>
                                                <td>{{$total}} Tk</td>
                                            </tr>
                                            <tr>
                                                <td>Shipping (Free)</td>
                                                <td>0</td>
                                            </tr>
                                            <tr class="total">
                                                <td>Total</td>
                                                <td class="total-amount">{{$total}} Tk</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <section><button class="btn btn__bg d-block" type="submit" style="width: 457.5px;border: .1px solid #918e8e;">Confirm Your Order</button></section>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            
       	</form>    
		</div>
 
        </div>
 
        <!-- order main wrapper end -->


@stop





