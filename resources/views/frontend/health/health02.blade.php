@extends('frontend.master')

@section('content')




<!-- main wrapper start -->
    <main class="body-bg">

        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <h1>blog details</h1>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                    <li class="breadcrumb-item"><a href="blog-left-sidebar.html">blog</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">blog details</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->

        <!-- blog main wrapper start -->
        <div class="blog-main-wrapper pt-50 pb-50">
            <div class="container">
                <div class="row">
                    
                    <div class="col-12">
                        <!-- blog single item start -->
                        <div class="blog-post-item blog-grid section-bg-color">
                            <div class="blog-post-thumb">
                                <div class="blog-carousel-active slick-arrow-style slick-dot-style">
                                    <div class="blog-single-slide">
                                        <img src="{{url('images/blog-2.jpg')}}" alt="" style="height: 500px; width: 100%">
                                    </div>
                                    
                                    
                                </div>
                            </div>
                            <div class="post-info-wrapper">
                                <div class="entry-header">
                                    <div class="post-date">
                                        <span class="date">17</span>
                                        <span class="month">mar</span>
                                    </div>
                                    <div class="post-meta blog-details-title">
                                        <h2 class="entry-title">Seven-day diabetes meal plan</h2>
                                        <div class="post-meta-small">
                                            <div class="post-author">
                                                Written By: Danielle Dresden
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="entry-summary mb-0">
                                    
                                    <blockquote>
                                        <p>Managing blood sugar levels is the key to living well with diabetes and avoiding some of the more severe health problems it can cause. This means that following a healthful diet is essential for people with diabetes.</p>
                                    </blockquote>
                                    <p><h5>1,600 calorie plan: Saturday</h5><br><b>Breakfast: </b>1 cup low fat plain Greek yogurt sweetened with half a banana mashed, 1 cup berries, 1 Tbsp chia seeds <br><br>

									<b>Lunch: </b>Tacos: two corn tortillas, 1/3 cup cooked black beans, 1 oz low fat cheese, 2 Tbsp avocado, 1 cup coleslaw, salsa as dressing, two small plums <br><br>

									<b>Snack: </b>One cherry tomato and 10 baby carrots with 2 Tbsp hummus <br><br>

									<b>Dinner: </b>6 oz baked potato, 2 oz London broil, 1 tsp butter, 1 ½ cup steamed broccoli with 1 tsp nutritional yeast sprinkled on top, 1 ¼ cup whole strawberries <br><br>

									<b>Snack: </b>Half a small avocado drizzled with hot sauce</p> <br>
									<p><h5>1,600 calorie plan: Sunday</h5><br><b>Breakfast: </b>1 Chocolate peanut oatmeal: 1 cup cooked oatmeal, 1 scoop chocolate vegan or whey protein powder, 1 ½ Tbsp peanut butter, 1 Tbsp chia seeds <br><br>

									<b>Lunch: </b>One whole wheat pita pocket, ½ cup cucumbers, ½ cup tomatoes, ½ cup lentils, ½ cup leafy greens, 3 Tbsp salad dressing <br><br>

									<b>Snack: </b>1 oz pumpkin seeds, one medium apple<br><br>

									<b>Dinner: </b>3 oz boiled shrimp, 1 cup green peas, 1 tsp. butter, ½ cup cooked beets, 1 cup sautéed Swiss chard, 1 tsp balsamic vinegar <br><br>

									<b>Snack: </b>Half a small avocado drizzled with hot sauce</p> <br>
									<p><h5>1,200 calorie plan: Monday</h5><br><b>Breakfast: </b> One poached egg and ½ small avocado spread on one slice Ezekiel bread, one orange<br><br>

									<b>Lunch: </b> Mexican bowl: 1/3 cup brown rice, 1/3 cup beans, 1 cup chopped spinach, ¼ cup chopped tomatoes, ¼ cup bell peppers, 1 oz cheese, fresh salsa as sauce<br><br>

									<b>Snack: </b>One cherry tomato and 10 baby carrots with 2 Tbsp hummus <br><br>

									<b>Dinner: </b>1 cup cooked bean or lentil pasta, 1 ½ cups veggie tomato sauce (cook garlic, mushrooms, greens, zucchini, and eggplant into it), 2 oz ground lean turkey, one slice honeydew<br><br>

									<b>Snack: </b>Half a small avocado drizzled with hot sauce</p> <br>
									<p><h5>1,200 calorie plan: Tuesday</h5><br><b>Breakfast: </b>1 cup cooked oatmeal, ¾ cup blueberries, 1 oz almonds, 1 tsp chia seeds<br><br>

									<b>Lunch: </b>Salad: 2 cups fresh spinach, 2 oz grilled chicken breast, ½ cup chickpeas, ½ small avocado, ¾ cup sliced strawberries, ¼ cup shredded carrots, 2 Tbsp dressing<br><br>

									<b>Snack: </b>One cherry tomato and 10 baby carrots with 2 Tbsp hummus <br><br>

									<b>Dinner: </b>Mediterranean couscous: 2/3 cup whole wheat couscous, ½ cup sautéed eggplant, 2 Tbsp sundried tomatoes, five Kalamata olives chopped, 1 Tbsp balsamic vinegar, fresh basil<br><br>

									<b>Snack: </b>Half a small avocado drizzled with hot sauce</p> <br>
									<p><h5>1,200 calorie plan: Wednesday</h5><br><b>Breakfast: </b>Omelet: one whole egg, two egg white veggie omelet (spinach, mushrooms, bell pepper, avocado) with ½ cup black beans, 1 cup berries<br><br>

									<b>Lunch: </b> Sandwich: two slices high-fiber whole grain bread, 1 Tbsp Greek yogurt and 1 Tbsp mustard, 2 oz canned tuna in water mixed with shredded carrots, dill relish, 1 cup sliced tomato, one small apple<br><br>

									<b>Snack: </b>1 cup kefir<br><br>

									<b>Dinner: </b>½ cup succotash, 1 ½ oz cornbread, 1 tsp. butter, 2 oz pork tenderloin, 1 cup cooked asparagus, ½ cup fresh pineapple<br><br>

									<b>Snack: </b>Half a small avocado drizzled with hot sauce</p> <br>
									<p><h5>1,200 calorie plan: Thursday</h5><br><b>Breakfast: </b>1 cup low fat plain Greek yogurt sweetened with half a banana mashed, 1 cup berries, 1 Tbsp chia seeds <br><br>

									<b>Lunch: </b>Sweet potato toast: two slices toasted sweet potato, topped with 1 oz goat cheese, spinach, and 1 tsp sprinkled flaxseed<br><br>

									<b>Snack: </b>One cherry tomato and 10 baby carrots with 2 Tbsp hummus <br><br>

									<b>Dinner: </b> 2 oz rotisserie chicken, 1 cup raw cauliflower, 1 Tbsp salad dressing, 1 cup fresh berries<br><br>

									<b>Snack: </b>Half a small avocado drizzled with hot sauce</p> <br>
									<p><h5>1,600 calorie plan: Friday</h5><br><b>Breakfast: </b>1 cup low fat plain Greek yogurt sweetened with half a banana mashed, 1 cup berries, 1 Tbsp chia seeds <br><br>

									<b>Lunch: </b>Tacos: two corn tortillas, 1/3 cup cooked black beans, 1 oz low fat cheese, 2 Tbsp avocado, 1 cup coleslaw, salsa as dressing, two small plums <br><br>

									<b>Snack: </b>One cherry tomato and 10 baby carrots with 2 Tbsp hummus <br><br>

									<b>Dinner: </b>6 oz baked potato, 2 oz London broil, 1 tsp butter, 1 ½ cup steamed broccoli with 1 tsp nutritional yeast sprinkled on top, 1 ¼ cup whole strawberries <br><br>

									<b>Snack: </b>Half a small avocado drizzled with hot sauce</p> <br>
									
                                    
                                    <div class="tag-line">
                                        
                                    </div>
                                    <div class="blog-share-link">
                                        <h5>share :</h5>
                                        <div class="blog-social-icon">
                                            <a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a>
                                            <a href="#" class="twitter"><i class="fab fa-twitter"></i></a>
                                            <a href="#" class="pinterest"><i class="fab fa-pinterest-p"></i></a>
                                            <a href="#" class="google"><i class="fab fa-google-plus-g"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- blog single item end -->

                        <!-- comment area start -->
                        <!-- <div class="comment-section section-bg-color mt-50">
                            <h3>03 comment</h3>
                            <ul>
                                <li>
                                    <div class="author-avatar">
                                        <img src="assets/img/blog/comment-icon.png" alt="">
                                    </div>
                                    <div class="comment-body">
                                        <span class="reply-btn"><a href="#">reply</a></span>
                                        <h5 class="comment-author">admin</h5>
                                        <div class="comment-post-date">
                                            20 nov, 2019 at 9:30pm
                                        </div>
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim maiores
                                            adipisci optio ex, laboriosam facilis non pariatur itaque illo sunt?</p>
                                    </div>
                                </li>
                                <li class="comment-children">
                                    <div class="author-avatar">
                                        <img src="assets/img/blog/comment-icon.png" alt="">
                                    </div>
                                    <div class="comment-body">
                                        <span class="reply-btn"><a href="#">reply</a></span>
                                        <h5 class="comment-author">admin</h5>
                                        <div class="comment-post-date">
                                            20 nov, 2019 at 9:30pm
                                        </div>
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim maiores
                                            adipisci optio ex, laboriosam facilis non pariatur itaque illo sunt?</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="author-avatar">
                                        <img src="assets/img/blog/comment-icon.png" alt="">
                                    </div>
                                    <div class="comment-body">
                                        <span class="reply-btn"><a href="#">reply</a></span>
                                        <h5 class="comment-author">admin</h5>
                                        <div class="comment-post-date">
                                            20 nov, 2019 at 9:30pm
                                        </div>
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim maiores
                                            adipisci optio ex, laboriosam facilis non pariatur itaque illo sunt?</p>
                                    </div>
                                </li>
                            </ul>
                        </div> -->
                        <!-- comment area end -->

                        <!-- start blog comment box -->
                        <!-- <div class="blog-comment-wrapper section-bg-color mt-50">
                            <h3>leave a reply</h3>
                            <p>Your email address will not be published. Required fields are marked *</p>
                            <form action="#">
                                <div class="comment-post-box">
                                    <div class="row">
                                        <div class="col-12">
                                            <label>comment</label>
                                            <textarea name="commnet" placeholder="Write a comment"></textarea>
                                        </div>
                                        <div class="col-lg-4 col-md-4">
                                            <label>Name</label>
                                            <input type="text" class="coment-field" placeholder="Name">
                                        </div>
                                        <div class="col-lg-4 col-md-4">
                                            <label>Email</label>
                                            <input type="text" class="coment-field" placeholder="Email">
                                        </div>
                                        <div class="col-lg-4 col-md-4">
                                            <label>Website</label>
                                            <input type="text" class="coment-field" placeholder="Website">
                                        </div>
                                        <div class="col-12">
                                            <div class="coment-btn">
                                                <input class="btn btn__bg" type="submit" name="submit" value="Post Comment">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div> -->
                        <!-- start blog comment box -->
                    </div>
                </div>
            </div>
        </div>
        <!-- blog main wrapper end -->

    </main>
    <!-- main wrapper end -->


@endsection