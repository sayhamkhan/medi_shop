@extends('frontend.master')

@section('content')




<!-- main wrapper start -->
    <main class="body-bg">

        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <h1>blog details</h1>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                    <li class="breadcrumb-item"><a href="blog-left-sidebar.html">blog</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">blog details</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->

        <!-- blog main wrapper start -->
        <div class="blog-main-wrapper pt-50 pb-50">
            <div class="container">
                <div class="row">
                    
                    <div class="col-12">
                        <!-- blog single item start -->
                        <div class="blog-post-item blog-grid section-bg-color">
                            <div class="blog-post-thumb">
                                <div class="blog-carousel-active slick-arrow-style slick-dot-style">
                                    <div class="blog-single-slide">
                                        <img src="{{url('images/blog-1.jpg')}}" alt="" style="height: 500px; width: 100%">
                                    </div>
                                    
                                    
                                </div>
                            </div>
                            <div class="post-info-wrapper">
                                <div class="entry-header">
                                    <div class="post-date">
                                        <span class="date">10</span>
                                        <span class="month">mar</span>
                                    </div>
                                    <div class="post-meta blog-details-title">
                                        <h2 class="entry-title">Top Ten Nutrition Tips for Everyday Health</h2>
                                        <div class="post-meta-small">
                                            <div class="post-author">
                                                Written By: Stacy Kennedy, MPH, RD, CSO, LDN; Reboot Nutritionist
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="entry-summary mb-0">
                                    
                                    <blockquote>
                                        <p>Here are my top ten favorite tips for healthy eating all year long, with lots of amazing links to more helpful information from Reboot.  This is by no means a complete list of guidelines but a few key things to get you started or keep you moving forward on the path toward healthy eating as a lifestyle.</p>
                                    </blockquote>
                                    <p><h5>1.) Drink plenty of water.</h5><br>Our bodies are about 60% water – with muscle mass carrying much more than fat tissue!  We need to drink water to keep our body systems running smoothly, optimize metabolism, boost energy levels, and promote good digestion, just to name a few.  Besides water, electrolytes are important especially if you exercise.</p> <br>
                                    <p><h5></h5><br></p> <br>
                                    <p><h5>2.) Eat plenty of plants.</h5><br>These colorful gems provide essential phytonutrients, micronutrients, vitamins, minerals and enzymes – all of which are just as important for your health as the macronutrients we often hear about (think carbs, proteins and fats).</p> <br>
                                    <p><h5>3.) Eat and drink often throughout the day.</h5><br>The jury is still out on whether 6 small meals or 3 meals is best so try to figure out what feels right for you.  But overall, having high quality small snacks, “mini meals” or fresh juice during the day can help to boost energy and prevent over-eating.</p> <br>
                                    <p><h5>4.) Eat mindfully. </h5><br>Limit distractions and take time to experience eating and engage your senses. Up to 30-40% of nutrients may not be properly absorbed if you are distracted while eating.  Like walking, watching TV, typing, working – all very common eating activities these days.  Digestion begins in the brain so by looking at, thinking about and smelling your food, you can help your body benefit from the wonderful nutrients locked away in that meal while enjoying the experience even more!</p> <br>
                                    <p><h5>5.) Limit processed foods.</h5><br>Read labels carefully. Make natural, homemade versions of store-bought foods.  Like hummus or granola bars, yum!</p> <br>
                                    <p><h5>6.) Seek local foods often and organic foods sometimes.</h5><br>Local eating not only has more nutrients it can also save you money.  You don’t have to get everything organic if that isn’t feasible, for some items it matters more (like apples and strawberries).</p> <br>
                                    <p><h5>7.) Include healthy fats in your diet.</h5><br>Eating fat doesn’t necessarily make you fat!  Many immune supportive vitamins, like Vitamin E or beta-carotene and hormones, like Vitamin D require some fat in the diet for absorption.  Pass the avocado, please!</p> <br>
                                    <p><h5>8.) Include healthy protein rich foods, including plant-based choices.</h5><br>Protein rich foods can help to reduce reflux and keep blood sugar levels stable while supporting healthy muscles and your immune system. If you want to add more protein to your juices, chia seeds, hemp seeds, spirulina or the Reboot with Joe Protein Powder are great choices.</p> <br>
                                    <p><h5>9.) For weight management focus more on inclusion of healthy foods and less on restriction of portions.</h5></p> <br>
                                    <h5>10.) Enjoy eating! </h5><br>
                                    <!-- <p>Tell us, what are your favorite nutrition tips?!!</p> -->
                                    <div class="tag-line">
                                        
                                    </div>
                                    <div class="blog-share-link">
                                        <h5>share :</h5>
                                        <div class="blog-social-icon">
                                            <a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a>
                                            <a href="#" class="twitter"><i class="fab fa-twitter"></i></a>
                                            <a href="#" class="pinterest"><i class="fab fa-pinterest-p"></i></a>
                                            <a href="#" class="google"><i class="fab fa-google-plus-g"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- blog single item end -->

                        <!-- comment area start -->
                        <!-- <div class="comment-section section-bg-color mt-50">
                            <h3>03 comment</h3>
                            <ul>
                                <li>
                                    <div class="author-avatar">
                                        <img src="assets/img/blog/comment-icon.png" alt="">
                                    </div>
                                    <div class="comment-body">
                                        <span class="reply-btn"><a href="#">reply</a></span>
                                        <h5 class="comment-author">admin</h5>
                                        <div class="comment-post-date">
                                            20 nov, 2019 at 9:30pm
                                        </div>
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim maiores
                                            adipisci optio ex, laboriosam facilis non pariatur itaque illo sunt?</p>
                                    </div>
                                </li>
                                <li class="comment-children">
                                    <div class="author-avatar">
                                        <img src="assets/img/blog/comment-icon.png" alt="">
                                    </div>
                                    <div class="comment-body">
                                        <span class="reply-btn"><a href="#">reply</a></span>
                                        <h5 class="comment-author">admin</h5>
                                        <div class="comment-post-date">
                                            20 nov, 2019 at 9:30pm
                                        </div>
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim maiores
                                            adipisci optio ex, laboriosam facilis non pariatur itaque illo sunt?</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="author-avatar">
                                        <img src="assets/img/blog/comment-icon.png" alt="">
                                    </div>
                                    <div class="comment-body">
                                        <span class="reply-btn"><a href="#">reply</a></span>
                                        <h5 class="comment-author">admin</h5>
                                        <div class="comment-post-date">
                                            20 nov, 2019 at 9:30pm
                                        </div>
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim maiores
                                            adipisci optio ex, laboriosam facilis non pariatur itaque illo sunt?</p>
                                    </div>
                                </li>
                            </ul>
                        </div> -->
                        <!-- comment area end -->

                        <!-- start blog comment box -->
                        <!-- <div class="blog-comment-wrapper section-bg-color mt-50">
                            <h3>leave a reply</h3>
                            <p>Your email address will not be published. Required fields are marked *</p>
                            <form action="#">
                                <div class="comment-post-box">
                                    <div class="row">
                                        <div class="col-12">
                                            <label>comment</label>
                                            <textarea name="commnet" placeholder="Write a comment"></textarea>
                                        </div>
                                        <div class="col-lg-4 col-md-4">
                                            <label>Name</label>
                                            <input type="text" class="coment-field" placeholder="Name">
                                        </div>
                                        <div class="col-lg-4 col-md-4">
                                            <label>Email</label>
                                            <input type="text" class="coment-field" placeholder="Email">
                                        </div>
                                        <div class="col-lg-4 col-md-4">
                                            <label>Website</label>
                                            <input type="text" class="coment-field" placeholder="Website">
                                        </div>
                                        <div class="col-12">
                                            <div class="coment-btn">
                                                <input class="btn btn__bg" type="submit" name="submit" value="Post Comment">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div> -->
                        <!-- start blog comment box -->
                    </div>
                </div>
            </div>
        </div>
        <!-- blog main wrapper end -->

    </main>
    <!-- main wrapper end -->


@endsection