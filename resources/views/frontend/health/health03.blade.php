@extends('frontend.master')

@section('content')




<!-- main wrapper start -->
    <main class="body-bg">

        <!-- breadcrumb area start -->
        <div class="breadcrumb-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumb-wrap">
                            <nav aria-label="breadcrumb">
                                <h1>blog details</h1>
                                <ul class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                    <li class="breadcrumb-item"><a href="blog-left-sidebar.html">blog</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">blog details</li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb area end -->

        <!-- blog main wrapper start -->
        <div class="blog-main-wrapper pt-50 pb-50">
            <div class="container">
                <div class="row">
                    
                    <div class="col-12">
                        <!-- blog single item start -->
                        <div class="blog-post-item blog-grid section-bg-color">
                            <div class="blog-post-thumb">
                                <div class="blog-carousel-active slick-arrow-style slick-dot-style">
                                    <div class="blog-single-slide">
                                        <img src="{{url('images/blog-3.jpg')}}" alt="" style="height: 500px; width: 100%">
                                    </div>
                                    
                                    
                                </div>
                            </div>
                            <div class="post-info-wrapper">
                                <div class="entry-header">
                                    <div class="post-date">
                                        <span class="date">05</span>
                                        <span class="month">apr</span>
                                    </div>
                                    <div class="post-meta blog-details-title">
                                        <h2 class="entry-title">Kidney Failure (ESRD) Causes, Symptoms, & Treatments</h2>
                                        <div class="post-meta-small">
                                            <div class="post-author">
                                                Written By: Stacy Kennedy, MPH, RD, CSO, LDN; Reboot Nutritionist
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="entry-summary mb-0">
                                    
                                    <blockquote>
                                        <p>Kidney failure, also called end-stage renal disease (ESRD), is the last stage of chronic kidney disease. When your kidneys fail, it means they have stopped working well enough for you to survive without dialysis or a kidney transplant.</p>
                                    </blockquote>
                                    <p><h5>What causes kidney failure?</h5><br>In most cases, kidney failure is caused by other health problems that have done permanent damage (harm) to your kidneys little by little, over time.

									When your kidneys are damaged, they may not work as well as they should. If the damage to your kidneys continues to get worse and your kidneys are less and less able to do their job, you have chronic kidney disease. Kidney failure is the last (most severe) stage of chronic kidney disease. This is why kidney failure is also called end-stage renal disease, or ESRD for short.</p> <br>
                                    <p><h5></h5><br></p> <br>
                                    <p><h5>Symptoms of chronic kidney disease</h5><br>Chronic kidney disease (CKD) usually gets worse slowly, and symptoms may not appear until your kidneys are badly damaged. In the late stages of CKD, as you are nearing kidney failure (ESRD), you may notice symptoms that are caused by waste and extra fluid building up in your body.</p> <br>
                                    <p><h5>Treatment of kidney failure</h5><br>If you have kidney failure (end-stage renal disease or ESRD), you will need dialysis or a kidney transplant to live. There is no cure for ESRD, but many people live long lives while on dialysis or after having a kidney transplant.

									There are just a few options for treating kidney failure, including kidney transplant and several types of dialysis. Your doctor can help you figure out which treatment is best for you.</p> <br>
                                    <p><h5>New to dialysis</h5><br>Starting dialysis often means creating a new normal for yourself and your family. There’s a lot to think about, from choosing a treatment option, to finding new ways to enjoy your favorite activities, to managing a new diet. The FIRST30 program is all about helping you through this period of adjustment. Here, you’ll find videos featuring people like you, who once were new to dialysis, as well as a checklist of important questions to ask your health care team.</p> <br>
                                    <p><h5>Adjusting to kidney failure</h5><br>Learning that you have kidney failure can come as a shock, even if you have known for a long time that your kidneys were not working well. Having to change your lifestyle to make time for your treatments can make coping with this new reality even harder. You may have to stop working or find new ways to exercise. You may feel sad or nervous. All is not lost. You can get help to feel better and have a fulfilling life. Learn more about adjusting to living with kidney failure.</p> <br>
                                    <p><h5>Complications of kidney failure</h5><br>Your kidneys do many jobs to keep you healthy. Cleaning your blood is only one of their jobs. They also control chemicals and fluids in your body, help control your blood pressure and help make red blood cells. Dialysis can do only some, not all, of the jobs that healthy kidneys do. Therefore, even when you are being treated for kidney failure, you may have some problems that come from having kidneys that don’t work well. Learn more about the complications of kidney failure.</p> <br>
                                    <div class="tag-line">
                                        
                                    </div>
                                    <div class="blog-share-link">
                                        <h5>share :</h5>
                                        <div class="blog-social-icon">
                                            <a href="#" class="facebook"><i class="fab fa-facebook-f"></i></a>
                                            <a href="#" class="twitter"><i class="fab fa-twitter"></i></a>
                                            <a href="#" class="pinterest"><i class="fab fa-pinterest-p"></i></a>
                                            <a href="#" class="google"><i class="fab fa-google-plus-g"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- blog single item end -->
                    </div>
                </div>
            </div>
        </div>
        <!-- blog main wrapper end -->

    </main>
    <!-- main wrapper end -->


@endsection